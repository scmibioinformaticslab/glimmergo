from os import listdir, system, remove
from os.path import isfile, join, exists

class glimmerGo:
	# ==== CLASS ELEMENTS ====
	# @subjectFullPath - full or relative path to suoutputFullPath = 'bject or reference folder
	subjectFullPath = ''
	queryFullPath = ''
	outputFullPath = ''
	longOrfPath = ''
	trainingSetPath = ''
	icmPath = ''
	glimmerPath = ''
	glimmerParameterString = ''

	subjectFilenameList = []
	queryFilenameList = []
	outputFilenameList = []
	longOrfFilenameList = []
	trainingSetFilenameList = []
	icmFilenameList = []

	outputColumn = ['orf_id', 'start', 'end', 'frame', 'score']


	# ==== CONSTRUCTOR ====
	# Initiate class with required parameters and load all filenames
	# ==== PARAMETERS ====
	# @subjectFullPath - full or relative path to subject or reference folder
	def __init__(self, subjectFullPath = '', queryFullPath = '', outputFullPath = '', longOrfPath = '', trainingSetPath = '', icmPath = '', glimmerPath = '', glimmerParameterString = ''):
		# assigning class data
		self.subjectFullPath = subjectFullPath
		self.queryFullPath = queryFullPath
		self.outputFullPath = outputFullPath
		self.longOrfPath = longOrfPath
		self.trainingSetPath = trainingSetPath
		self.icmPath = icmPath
		self.glimmerPath = glimmerPath
		self.glimmerParameterString = glimmerParameterString

		# check if paths ends with '/'
		# if not, add '/' to path data
		if subjectFullPath and not subjectFullPath[-1] == '/':
			self.subjectFullPath += '/'
		if queryFullPath and not queryFullPath[-1] == '/':
			self.queryFullPath += '/'
		if outputFullPath and not outputFullPath[-1] == '/':
			self.outputFullPath += '/'
		if longOrfPath and not longOrfPath[-1] == '/':
			self.longOrfPath += '/'
		if trainingSetPath and not trainingSetPath[-1] == '/':
			self.trainingSetPath += '/'
		if icmPath and not icmPath[-1] == '/':
			self.icmPath += '/'
		if glimmerPath and not glimmerPath[-1] == '/':
			self.glimmerPath += '/'

		# load list of subject and query filenames into class
		self.loadFilenameList('subject')
		self.loadFilenameList('query')

	# ==== loadFilenameList METHOD ====
	# Initiate class with required parameters and load all filenames
	def loadFilenameList(self, list_indicator = False):
		# ---- VARIABLE EXPLANATION ----
		# @f - temporary variable to keep each filename while checking
		# ------------------------------
		# printing progress to terminal
		if list_indicator == False or list_indicator == 'subject':
			print '|'
			print '| Loading file list from Subject input folder.'
			# list everything from @subjectFullPath and append to @ subjectFilenameList if it is a file
			self.subjectFilenameList = [ f for f in listdir(self.subjectFullPath) if isfile(join(self.subjectFullPath, f)) ]
		if list_indicator == False or list_indicator == 'query':
			print '|'
			print '| Loading file list from Query input folder.'
			# list everything from @queryFullPath and append to @ queryFilenameList if it is a file
			self.queryFilenameList = [ f for f in listdir(self.queryFullPath) if isfile(join(self.queryFullPath, f)) ]
		if list_indicator == False or list_indicator == 'output':
			print '|'
			print '| Loading file list from output folder.'
			# list everything from @outputFullPath and append to @ outputFilenameList if it is a file
			self.outputFilenameList = [ f for f in listdir(self.outputFullPath) if isfile(join(self.outputFullPath, f)) ]
		if list_indicator == False or list_indicator == 'icm':
			print '|'
			print '| Loading file list from icm folder.'
			# list everything from @icmPath and append to @icmFilenameList if it is a file
			self.icmFilenameList = [ f for f in listdir(self.icmPath) if isfile(join(self.icmPath, f)) ]
		if list_indicator == False or list_indicator == 'longorf':
			print '|'
			print '| Loading file list from long orfs folder.'
			# list everything from @longOrfPath and append to @longOrfFilenameList if it is a file
			self.longOrfFilenameList = [ f for f in listdir(self.longOrfPath) if isfile(join(self.longOrfPath, f)) ]
		if list_indicator == False or list_indicator == 'trainingset':
			print '|'
			print '| Loading file list from training set folder.'
			# list everything from @trainingSetPath and append to @trainingSetFilenameList if it is a file
			self.trainingSetFilenameList = [ f for f in listdir(self.trainingSetPath) if isfile(join(self.trainingSetPath, f)) ]

	# ==== getLongORF METHOD ====
	# 
	def getLongORF(self):
		longorf_parameter = '-n -t 1.15 -z 11'
		# longorf_parameter = '-z 11'
		for a_subject in self.subjectFilenameList:
			strain_id = a_subject[ : a_subject.find('.')]
			print 'getting long, non-overlapping ORFs of ' + a_subject
			command = '%slong-orfs %s %s %s.longorf' % (self.glimmerPath, longorf_parameter, join(self.subjectFullPath, a_subject), join(self.longOrfPath, strain_id))
			# print command
			system(command)
			self.loadFilenameList('longorf')

	# ==== buildICM METHOD ====
	# 
	def buildICM(self, text_out = False):
		# build_icm_parameter = ''
		# build_icm_parameter = '-z 11'

		self.loadFilenameList('trainingset')

		for a_subject in self.trainingSetFilenameList:
			strain_id = a_subject[ : a_subject.find('.')]
			print 'building ICM for ' + a_subject
			if text_out == True:
				command = '%sbuild-icm %s -t %s.icm.txt < %s' % (self.glimmerPath, build_icm_parameter, join(self.icmPath, strain_id), join(self.trainingSetPath, a_subject))
			else:
				command = '%sbuild-icm %s %s.icm < %s' % (self.glimmerPath, build_icm_parameter, join(self.icmPath, strain_id), join(self.trainingSetPath, a_subject))
			# print command
			system(command)
			self.loadFilenameList('icm')

	# ==== runGlimmer METHOD ====
	# Initiate class with required parameters and load all filenames
	def runGlimmer(self):
		# ---- VARIABLE EXPLANATION ----
		# @
		# ------------------------------
		for a_query in self.queryFilenameList:
			strain_id = a_query[ : a_query.find('.')]
			for a_icm in self.icmFilenameList:
				command = '%sglimmer3 %s %s %s %s' % (self.glimmerPath, self.glimmerParameterString, join(self.queryFullPath, a_query), join(self.icmPath, a_icm), join(self.outputFullPath, strain_id))
				system(command)

	# ==== parsePredictOutput METHOD ====
	# 
	def parsePredictOutput(self):
		self.loadFilenameList('output')
		predict_output = [an_output for an_output in self.outputFilenameList if an_output.endswith('.predict')]
		
		prediction_result_dict = {}
		for an_output in predict_output:
			output_id = an_output[: an_output.find('.')]
			prediction_result_dict[output_id] = {}

			with open(join(self.outputFullPath, an_output)) as predict_file:
				for a_line in predict_file:
					a_line = a_line.strip('\n')
					if a_line.startswith('>'):
						# This line is sequence id
						sequence_name = a_line.strip('>')
						prediction_result_dict[output_id][sequence_name] = []
					else:
						# This line is glimmer prediction result
						a_line = a_line.split(' ')
						a_line = [a_column for a_column in a_line if a_column != '']
						prediction_result_dict[output_id][sequence_name].append(a_line)

		return prediction_result_dict

	def parseLongOrfOutput(self):
		longorf_result_dict = {}

		for longorf_filename in self.longOrfFilenameList:
			output_id = longorf_filename[: longorf_filename.find('.')]
			longorf_result_dict[output_id] = {}

			with open(join(self.longOrfPath, longorf_filename), 'r') as longorf_file:
				longorf_result_dict[output_id][output_id] = []
				for a_line in longorf_file:
					if a_line[0].isdigit():
						# This is the data line
						a_line = a_line.strip('\n').split(' ')
						a_line = [a_column for a_column in a_line if a_column != '']
							
						longorf_result_dict[output_id][output_id].append(a_line)

		return longorf_result_dict
